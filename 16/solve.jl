# Show only the lines that don't go against the 'lookup' rules
# hopefully there will be only one! 

lookup = [("children: 3", "children:"),
("cats: 7", "cats:"),
("samoyeds: 2", "samoyeds:"),
("pomeranians: 3", "pomeranians:"),
("akitas: 0", "akitas:"),
("vizslas: 0", "vizslas:"),
("goldfish: 5", "goldfish:"),
("trees: 3", "trees:"),
("cars: 2", "cars:"),
("perfumes: 1", "perfumes:")]

open("input","r") do f
    for line in eachline(f)
        good=true
        for t in lookup
            # if string containst i.e. "cats:", but nor "cats: 7" => baaad
            if(  searchindex(line, t[2])>0 && searchindex(line, t[1])==0 )
                good=false
                break
            end
        end
        if good
            println("good line: ", line)
        end
    end
end
