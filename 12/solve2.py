import json

def get_value(v):
    if isinstance(v, int):
        return v
    elif isinstance(v, list):
        return sum_list(v)
    elif isinstance(v, dict):
        return sum_dict(v)
    return 0
        
def sum_list(l):
    total=0
    for v in l:
        total+=get_value(v)
    return total

def sum_dict(d):
    total=0
    for k,v in d.iteritems():
        if isinstance(v, unicode) and v=="red":
            return 0;
        total+=get_value(v)
    return total        

with open('input') as f:    
    d = json.load(f)
    v = get_value(d)
    print "Day 12, problem #2 = ", v
