

function coor = coords (token)
    coor = strsplit(token, ",");
    coor = [  str2num(coor{1}), str2num(coor{2})  ];
endfunction

fh = fopen ("input");

m = zeros(1000, 1000);
line = fgetl (fh);

do
    tokens = strsplit(line, " ");
    if( strcmp( tokens{1}, "toggle") )
        coor1 = coords( tokens{2} );
        coor2 = coords( tokens{4} );
        sizes = [coor2(1) - coor1(1)+1, coor2(2) - coor1(2)+1];
        m( coor1(1):coor2(1), coor1(2):coor2(2) ) = xor(  m( coor1(1):coor2(1), coor1(2):coor2(2) ), ones(sizes) );
    else
        coor1 = coords( tokens{3} );
        coor2 = coords( tokens{5} );
        sizes = [coor2(1) - coor1(1)+1, coor2(2) - coor1(2)+1];

        if( strcmp( tokens{2}, "on") )
            m( coor1(1):coor2(1), coor1(2):coor2(2) ) =  ones(sizes);
        else
            m( coor1(1):coor2(1), coor1(2):coor2(2) ) =  zeros(sizes);        
        endif

    endif
    
    line = fgetl (fh);
until (line==-1);

fclose (fh);

disp("Day 6, solution #1:"), disp(sum(sum(m)));



