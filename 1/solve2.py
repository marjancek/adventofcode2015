floor=0
pos=0
with open("input", "r") as f:
    byte = f.read(1)
    while byte != "":
        pos=pos+1
        if(byte=='('):
            floor=floor+1
        elif(byte==')'):
            floor=floor-1
        if(floor<0):
            print(pos)
            break      
        byte = f.read(1)
        
print(floor)
