# AdventOfCode, day 15. Using genetic algorithm to find second calorie-bounrd
# recipe MUCH FASTER than with brute force

# Get the data, strip the text, keep the numbers
load_data <- function(){
    f<-file('input')
    data <- readLines( f)
    close(f)
    data<-strsplit(gsub(",", "", data), " ")
    data<-matrix(unlist(data), ncol = 11, byrow = TRUE)
    data<-data[, c(3, 5, 7, 9, 11)]
    data<-matrix(as.numeric(data), ncol = 5, byrow = FALSE)
}

# score the goodness of the cookie
score <- function(quant){
    # data[,1:4] : ignore calories
    value<-apply(quant*data[,1:4], 2, sum)
    value[value<0]<-0
    value<-prod(value)
}

# create random factors that should add to 100 ( "Most of the time(C)" )
rand_factors<-function(n_ingredients, total)
{
    k<-rnorm(n_ingredients, mean=total, sd=total)
    k<-k*k
    k<-k/sum(k)*total
    k<-round(k)
}

# Move descretelly on teaspoon space keeing things at 100 tbs
# move in the direction that minimizes the difference with the 
# expected 500 calories. Hopefully we will reach exactly 500!
next_factors<-function(factors){
    best_calories<-(sum(data[,5]*factors)-500)**2
    best_factors<-factors
    s<-diag(length(factors))
    for(i in 1:(length(factors)-1) )
    {
        for(j in (i+1):length(factors))
        {
            this_factors<-factors-s[i,]+s[j,]
            if(sum(best_factors<0)==0)
            {
                this_calories<-(sum(data[,5]*this_factors)-500)**2
                if(this_calories==0){
                    return(this_factors)
                }
                if(best_calories>this_calories)
                {
                    best_calories<-this_calories
                    best_factors<-this_factors
                }
            }
        }
    }
#    sum(best_factors)
    if(sum(abs(best_factors-factors))==0)
        return(NA)
    return(best_factors)
}

# get a random start point and move on tbs space trying to reach 500 calories
# Check if it actually has 100 tbs and 500 calories (often will not), and pick
# the best it finds
random_search<-function(){
    n_ingredients<-dim(data)[1]
    factors<-rand_factors(n_ingredients, 100)

    best<- factors
    best_calories<-abs(sum(data[,5]*best)-500)
    best<-next_factors(factors)

    while(sum(is.na(best))==0 && best_calories && best)
    {
        next_best<-next_factors(best)
        if(sum(is.na(next_best))>0  )
            break
        next_calories<-abs(sum(data[,5]*next_best)-500)
        if(next_calories>=best_calories)
            break
        best<-next_best
        best_calories<-next_calories
    }

    best
}

data<-load_data()

best <- -1
best_factors<-c(0,0,0,0)

# find 100 tbs, 500 calories solutions and get the one with the higher score
# WARNING: the result might not be the actual best, but if enough interations
# are performed we hope to find it (AoC will tell us if we're not just there!)
set.seed(20151215)
iterations<-10000
while(iterations)
{
    factors<-random_search()
    calories<-sum(data[,5]*factors)
    if( sum(is.na(factors) )==0 && calories==500 && sum(factors)==100)
    {
        points<-score(factors)
        if(points>best){
            best<-points
            best_factors<-factors
            print(best)
            print(best_factors)
        }
    }
    iterations<-iterations-1
}

print(best)
print(best_factors)

