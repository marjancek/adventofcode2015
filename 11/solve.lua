--str = "hzzzzzzz"
--str = "iolaabcppp"
str = "aabxbsaak"


function next(str)
    local carry=1
    local last = string.byte("z")
    local result=""
    for i=#str,1,-1 do
        local c = string.byte( str:sub(i,i) )
        if(c==last and carry>0) then
            result = "a" .. result
            carry=1
        else
            result = string.char(c+carry) .. result
            carry=0
        end
    end
    return result
end


function forbidden(str)
    if( str:match( ".*[i%o%k].*") == nil) then
        return false
    end
    return true
end


function seq(str)
    for i=3, #str do
        local a = string.byte( str:sub(i-2,i-2) )
        local b = string.byte( str:sub(i-1,i-1) )
        local c = string.byte( str:sub(i,i) )
        if( (a+1==b) and (b+1==c) ) then
            return true
        end
    end
    return false
end


-- poor regular expresions support in lua :(
function pairs(str)
    a, b, c, d = str:match( "(.)%1.*(.)%2.*(.)%3.*(.)%4" )
    if(a) then
        return (not (a==b) or not (a==c) or not (a==d) or not (b==c) or not (b==d) or not (c==d) )
    end
    a, b, c = str:match( "(.)%1.*(.)%2.*(.)%3" )
    if(a) then
        return (not (a==b) or not (a==c) or not (b==c) )
    end
    a, b = str:match( "(.)%1.*(.)%2" )
    if(a) then
        return (not (a==b) )
    end
    return false
end

function test()
    print(str)
    n=next(str)
    print(n)
    print(forbidden(n))
    print(seq(n))
    
    a, b = n:match( ".*(.)%1.*(.)%2.*")
    print( a )
    print( b )
    
    print(  n:match( "(.)%1.*(^%1)%2")  )
    
    a, b, c, d = str:match( "(.)%1.*(.)%2.*(.)%3.*(.)%4" )
    print(a)
    print(b)
    print(c)
    print(d)
    
    
    print(pairs(str) )
end

-- First part
str = "hepxcrrq"
--str = "hepxxyzz"

repeat
    if(not forbidden(str) and pairs(str) and seq(str) ) then
        print(str)
        break
    else
        str=next(str)    
    end
until (false)

-- Second part
str=next(str) 

repeat
    if(not forbidden(str) and pairs(str) and seq(str) ) then
        print(str)
        break
    else
        str=next(str)    
    end
until (false)


