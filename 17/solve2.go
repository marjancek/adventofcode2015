package main
 
import (
    "bufio"
    "fmt"
    "io"
    "log"
    "os"
    "strconv"
    "sort"
)

// recursivelly count how many different ways to fill N containers, for all N
func count_fit(litres int, sizes[] int, count int, m map[int]int )  int {

    if(litres<=0){
        return m[count]
    }    
    if(sizes[0]>litres){
        return m[count]
    }
    if(len(sizes)==1){
        if(sizes[0]==litres){
            m[count+1]= m[count+1]+1
           return m[count+1]
        }
        return count
    }

    if(sizes[0]==litres){
        m[count+1]= m[count+1]+1
        return count_fit(litres, sizes[1:], count, m) +1
    }
    return count_fit(litres-sizes[0], sizes[1:], count+1, m)+count_fit(litres, sizes[1:], count, m)
}

func main() {
    total := 150
    file, err := os.Open("input")
    if err != nil {
        log.Fatal(err)
    }
    buff := bufio.NewReader(file)
    sizes := make([]int, 0)
    for {
        line, isPrefix, err := buff.ReadLine()
        if err == io.EOF {   break   }
        if isPrefix { log.Fatal("Error: Unexpected long line reading", file.Name()) }
        if err != nil {   log.Fatal(err)    }

        i, err := strconv.Atoi(string(line))
        sizes = append(sizes, i)
    }
    sort.Ints(sizes)
    m := map[int]int{}

    for i := 0; i <= len(sizes); i++ {
        m[i]=0
    }    

    count_fit(total, sizes, 0,  m)

    for i := 0; i <= len(sizes); i++ {
        if(m[i]>0){
        fmt.Print("Day 17 soltion #2: ")
        fmt.Print(strconv.Itoa(m[i]))
        fmt.Print("; that many different ways of using ")
        fmt.Print(strconv.Itoa(i))
        fmt.Println(" containers")
            break;
        }
    }        
}
