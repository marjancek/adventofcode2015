package main
 
import (
    "bufio"
    "fmt"
    "io"
    "log"
    "os"
    "strconv"
    "sort"
)

func count_fit(litres int, sizes[] int) int{
    if(litres<=0){
        return 0
    }    
    if(sizes[0]>litres){
        return 0
    }
    if(len(sizes)==1){
        if(sizes[0]==litres){   return 1    }
        return 0
    }

    if(sizes[0]==litres){
        return count_fit(litres, sizes[1:])+1
    }
    return count_fit(litres-sizes[0], sizes[1:])+count_fit(litres, sizes[1:])
}

func main() {
    total := 150
    file, err := os.Open("input")
    if err != nil {
        log.Fatal(err)
    }
    buff := bufio.NewReader(file)
    sizes := make([]int, 0)
    for {
        line, isPrefix, err := buff.ReadLine()
        if err == io.EOF {   break   }
        if isPrefix { log.Fatal("Error: Unexpected long line reading", file.Name()) }
        if err != nil {   log.Fatal(err)    }
        i, err := strconv.Atoi(string(line))
        sizes = append(sizes, i)
    }

    sort.Ints(sizes)

    count := count_fit(total, sizes)
    fmt.Print("Day 17 soltion #1: ")
    fmt.Println(strconv.Itoa(count))   
}
