An Advent calendar serves as a countdown from December 1st Christmas, usually one in which childen get a candy every day.

The [Advent Of Code](http://adventofcode.com/2015) is an Advent calendar for coders, with a new simple challenge each day until Christmas 2015.

I will try to use as many different languages as I can, including many I'm not particularly confortable with.
Hopefully some might serve as inspiration for your own nasty stuff.

* Dec 01: bash and python
* Dec 02: R
* Dec 03: nodejs
* Dec 04: C#
* Dec 05: C++ and perl
* Dec 06: Octave / Mathlab
* Dec 07: D
* Dec 08: ruby
* Dec 09: python+numpy (sorry; too much time on a plane without internet)
* Dec 10: Java
* Dec 11: lua (imperative)
* Dec 12: bash and python (again, the simple one with sed, the other with Python)
* Dec 13: python+numpy (again; recycling solution for day 9!)
* Dec 14: Scala
* Dec 15: R (linear optimization) / R (genetic algorithm)
* Dec 16: Julia
* Dec 17: Go (recursion)
* Dec 18: R, again (it called for it! very simple code)
* Dec 19: python (no time for anything else!) with stack recursion
* Dec 20: C (smarter/not so smart brute force)
* Dec 21: Pen and paper!!! (yes, that a way to solve problems too!!!)
* Dec 22: Object Oriented C (struct trick for inheritance!)
* Dec 23: python
* Dec 24: python (no time for programming !!!)
* Dec 25: Rust
